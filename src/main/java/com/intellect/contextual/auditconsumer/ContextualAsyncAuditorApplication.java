package com.intellect.contextual.auditconsumer;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = { "com.intellect.contextual"})
public class ContextualAsyncAuditorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContextualAsyncAuditorApplication.class, args);
	}

	 @Bean
	    public Jackson2JsonMessageConverter converter() {
	        return new Jackson2JsonMessageConverter();
	    }
	
}
